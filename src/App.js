import React, {useState} from "react";
import {BrowserRouter, Redirect, Route, Switch} from "react-router-dom";

import Items from "./components/Items";
import Auth from "./components/Auth";
import Header from "./components/Header";


const App = () => {
    const [] = useState()
    return (
        <>
            <h3>Container app!!!</h3>
            <BrowserRouter>
                <Header/>
                <div>
                    <Switch>
                        <Route path="/auth" component={Auth} />
                        <Route path="/" component={Items} />
                        <Redirect to="/"/>
                    </Switch>
                </div>
            </BrowserRouter>
        </>
    )
}

export default App
