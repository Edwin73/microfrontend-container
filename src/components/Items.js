import React, {useRef, useEffect} from "react";
import {ItemsReactComponent} from 'items/ItemsApp'


const Items = () => {
    const ref = useRef(null)

    useEffect(
        () => {
            ItemsReactComponent(ref.current)
        }, []
    )

    return <div ref={ref}/>
}

export default Items
