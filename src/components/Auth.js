import React, {useRef, useEffect} from "react"
import {AuthReactComponent} from 'auth/AuthApp'


const Auth = () => {
    const ref = useRef(null)

    useEffect(
        () => {
            AuthReactComponent(ref.current)
        }, []
    )

    return <div ref={ref}/>
}

export default Auth
