import React from "react";
import {Link} from 'react-router-dom';

const Header = () => (
    <div className="d-flex flex-row-reverse">
        <div className="p-2">
            <Link to="/auth">
                <button className="btn btn-warning">
                    Login
                </button>
            </Link>
        </div>
        <div className="p-2">
            <Link to="/">
                <button className="btn btn-secondary">
                    Items
                </button>
            </Link>
        </div>
    </div>
)

export default Header
